from setuptools import setup, find_packages

setup(
    name="Play2048",
    packages=find_packages(exclude=['*test']),
    version="0.1.1",
    author="Eric Wong, Dicky Li",
    description=' ',
    author_email=' ',
    install_requires=['numpy', 'tqdm', 'argparse', 'matplotlib'],
    entry_points={
        'console_scripts': [
            'Play2048 = Game2048.command:process'
        ]}
)
