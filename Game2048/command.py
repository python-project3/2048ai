from .class_2048 import Game_2048


def process():
    game1 = Game_2048()

    left = 0
    right = 1
    up = 2
    down = 3

    game1.new_game()
    print(game1.grid)
    game1.make_move(up)
    print(game1.score)
    print(game1.grid)
    game1.make_move(right)
    print(game1.score)
    print(game1.grid)
    game1.make_move(down)
    print(game1.score)
    print(game1.grid)
    game1.make_move(left)
    print(game1.score)
    print(game1.grid)
    game1.make_move(down)
    print(game1.score)
    print(game1.grid)

if __name__ == "__main__":
    process()
